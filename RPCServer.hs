--New Server
import EasyNetwork
import System.IO

main = do
    ip <- ipAddress
    putStr "Lets play Rock, Paper, Scissors!\nMy IP is: "
    putStrLn ip
    putStrLn "Waiting for connection..."
    conn <- server "12345"
    
    game conn 0 0
    
game :: Handle -> Int -> Int -> IO()
game conn player1score player2score = do
    clientname <- hGetLine conn
    putStr "Someone Connected! Your opponent is: "
    putStrLn clientname

    putStrLn "\nWhat is your name? Input:"
    myName <- getLine
    
    --Send name of Server (Player 1)
    hPutStrLn conn myName
    
    playRound conn player1score player2score myName clientname 
    
    


playRound :: Handle -> Int -> Int -> [Char] -> [Char] -> IO()
playRound conn player1score player2score myName clientName  = do
    --Tell the client its waiting
    hPutStrLn conn "newline"
    hPutStr conn "Waiting for "
    hPutStr conn myName
    hPutStrLn conn "..."
    
    --Server play (player 1)
    putStrLn "\nPlay! (r) for Rock, (p) for Paper and (s) for Scissors or (e) to exit. Your choice:"
    
    choice1 <- getLine


    
    
    --Client play (player 2)
    hPutStrLn conn "newline"
    hPutStrLn conn "Play! (r) for Rock, (p) for Paper and (s) for Scissors or (e) to exit. Your choice:"
    putStr "\nWaiting for "
    putStr clientName
    putStrLn "..."
    
    --Ask for input to be given
    hPutStrLn conn "giveinput"
    choice2 <- hGetLine conn

    putStrLn "\nOpponent chose "
    putStrLn choice2
    putStrLn " "
    
    hPutStrLn conn "newline"
    hPutStrLn conn "Opponent chose "
    hPutStrLn conn choice1
    hPutStrLn conn "newline"
    
    
    --WHO IS THE WINNER?
    let result = getWinner choice1 choice2
    
    --Change the score based on the winner           
    if result == "Player 1 Wins!"
        then do
            --Add to player 1's score
            let s1 = add1ToNum player1score
            
            --Print on server the scores/win
            putStrLn "Winner of Round:"
            putStrLn myName
            putStrLn "\nYour score: "
            printNumber s1
            putStrLn " "
            putStr clientName
            putStrLn "'s Score: "
            printNumber player2score
            
            
            --Print on client the scores/win
            hPutStrLn conn "Winner of Round:"
            hPutStrLn conn myName
            hPutStrLn conn "newline"
            hPutStrLn conn  "Your Score: "
            hPutStrLn conn (numToString player2score)
            hPutStrLn conn "newline"
            hPutStr conn myName
            hPutStrLn conn "'s Score: "
            hPutStrLn conn (numToString s1)      
            
            
            playRound conn s1 player2score myName clientName
        else if result == "Player 2 Wins!"
            then do
                --Add to player 1's score
                let s2 = add1ToNum player2score
                
                --Print on server the scores/win
                putStrLn "Winner of Round:"
                putStrLn clientName
                putStrLn "\nYour score: "
                printNumber player1score
                putStrLn " "
                putStr clientName
                putStrLn "'s Score: "
                printNumber s2
                
                
                --Print on client the scores/win
                hPutStrLn conn "Winner of Round:"
                hPutStrLn conn clientName
                hPutStrLn conn "newline"
                hPutStrLn conn  "Your Score: "
                hPutStrLn conn (numToString s2)
                hPutStrLn conn "newline"
                hPutStr conn myName
                hPutStrLn conn "'s Score: "
                hPutStrLn conn (numToString player1score)      
                
                
                playRound conn player1score s2 myName clientName
            else if result == "Its a tie!"
                then do
                                    
                    --Print on server the scores
                    putStrLn "Winner of Round:"
                    putStrLn "IT'S A TIE!"
                    putStrLn "\nYour score: "
                    printNumber player1score
                    putStrLn " "
                    putStr clientName
                    putStrLn "'s Score: "
                    printNumber player2score
                    
                    
                    --Print on client the scores
                    hPutStrLn conn "Winner of Round:"
                    hPutStrLn conn "IT'S A TIE"
                    hPutStrLn conn "newline"
                    hPutStrLn conn  "Your Score: "
                    hPutStrLn conn (numToString player2score)
                    hPutStrLn conn "newline"
                    hPutStr conn myName
                    hPutStrLn conn "'s Score: "
                    hPutStrLn conn (numToString player1score)      
                    
                    playRound conn player1score player2score myName clientName
                    
                    else do
                        hPutStrLn conn "Game Over!"
                        hPutStrLn conn "gameover"
                        putStrLn "Game Over!"
                
    


getWinner choice1 choice2 = 
    if choice1 == choice2
        then "Its a tie!"
        else if (choice1 == "r" && choice2 == "s") || (choice1 == "s" && choice2 == "p") || (choice1 == "p" && choice2 == "r")
            then "Player 1 Wins!"
            else if (choice1 == "e" || choice2 == "e")
                then "Game Quit"
                else "Player 2 Wins!"



numToString num =
    show num
    
printNumber num = do
    putStrLn (numToString num)

add1ToNum :: Int -> Int  
add1ToNum num = 
    num + 1
    
sub1ToNum :: Int -> Int  
sub1ToNum num = 
    num - 1
