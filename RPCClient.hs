--New Client
import EasyNetwork
import System.IO

main = do
    putStrLn "Hello! Let's Play Rock Paper Sisscors."
    putStrLn "You must be on the same network to play."
    putStrLn "Enter the IP address of the server: "

    ip <- getLine
    conn <- client ip "12345"
    loop conn

loop :: Handle -> IO()
loop conn = do
    --Get name of player and send to server
    putStrLn "\nWhat's your name? Input: "
    name <- getLine
    putStrLn "Connecting to server..."
    hPutStrLn conn name
    
    --Get oppenent name and start playing
    opName <- hGetLine conn
    putStr "You're Connected! Your opponent is: "
    putStrLn opName
    
    serverWait conn opName
    
    

serverWait :: Handle -> [Char] -> IO()
serverWait conn opName  = do
    message <- hGetLine conn
    
    if message == "giveinput"
        then do
            input <- getLine  
            hPutStrLn conn input 
        else if message == "gameover"
            then do
                putStrLn "Game is over! Closing connection"
            else if message == "newline"
                then do
                    putStrLn " "
                else do
                    putStrLn message
    
    serverWait conn opName
    
    
