# RockPaperScissors

![](https://www.snow.edu/pr/brand/images/signature.jpg)

##Survey of Languages

#####Content is from the course SE 3250 (Survey of Languages). This is a group project. Work in this repository is in the Haskell Programming language.

######Content in the "Stuff from Daniel" is from our Professor. He gave us the basic networking code.

This project gave an introduction to the Haskell programming language by creating a simple networked game of rock, paper, scissors!

Team Members:

- Kara Bayn: https://bitbucket.org/KaraBayn/

- Ty Bayn: https://bitbucket.org/tybayn/

- Jackson Porter: https://bitbucket.org/jacksonporter/

- Alex Thayn: https://bitbucket.org/alexthayn/

