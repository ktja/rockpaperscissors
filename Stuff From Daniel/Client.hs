import EasyNetwork
import System.IO 

main = do
  putStrLn "Enter Ip Address of the server"
  ip <- getLine
  conn <- client ip "12345"
  loop conn

loop :: Handle -> IO()
loop conn = do
  hPutStrLn conn "Ping"
  putStrLn "Sending Ping"
  resp <- hGetLine conn
  putStr "Got: "
  putStrLn resp
  loop conn
