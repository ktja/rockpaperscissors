import EasyNetwork
import System.IO 

main = do
  ip <- ipAddress
  putStr "Server Ip = "
  putStrLn ip
  conn <- server "12345"
  loop conn

loop :: Handle -> IO()
loop conn = do
  msg <- hGetLine conn
  putStr "Got: "
  putStrLn msg
  putStrLn "Sending Pong"
  hPutStrLn conn "Pong"
  loop conn
