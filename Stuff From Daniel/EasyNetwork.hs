module EasyNetwork where

import Network.Socket as Sock
import Network as Net
import System.IO 

client :: String -> String -> IO Handle
client ip port = do 
  let host = ip
      portName = Net.PortNumber (fromIntegral (read port :: Int))
  hdl <- Net.connectTo host portName
  hSetBuffering hdl NoBuffering
  return hdl

server :: String -> IO Handle
server port = do 
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet (fromIntegral (read port :: Int)) iNADDR_ANY)
  listen sock 2
  (conn, _) <- Sock.accept sock
  hdl <- socketToHandle conn ReadWriteMode
  hSetBuffering hdl NoBuffering
  return hdl

ipAddress :: IO String
ipAddress = do 
  sock <- socket AF_INET Datagram 0
  addrinfos <- getAddrInfo Nothing (Just "8.8.8.8") Nothing
  let serverAddr = head addrinfos
  connect sock (addrAddress serverAddr)
  name <- getSocketName sock
  return (takeWhile (/= ':') (show name))
